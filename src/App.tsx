import Header from './layouts/header';
import Main from './pages/trade'
import Settings from './pages/settings'

import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

function App() {
  return (
    <div className="w-screen h-screen">
      <div className="w-full">
        <Header />
      </div>
      <div className="w-full h-full">
        <Routes>
          <Route path="trade" element={<Main />} />
          <Route path="settings" element={<Settings />} />
        </Routes>
      </div>
    </div>
  );
}

export default App;
