import { configureStore } from '@reduxjs/toolkit'
import TickerReducer from './features/tickerList/tickerSlice'
import ActiveTabReducer from './slices/activeTab'

export const store = configureStore({
  reducer: {
      tickers: TickerReducer,
      activeTabs: ActiveTabReducer
  },
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch