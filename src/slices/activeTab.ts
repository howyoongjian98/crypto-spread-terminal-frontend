import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface navigation {
    name: string,
    href: string,
    current: boolean,
}

export const navigationList: navigation[] = [
    { name: 'Trade', href: 'trade', current: true },
    { name: 'Settings', href: 'settings', current: false },
]

interface navigationState {
    activeTab: string,
}

const initialState: navigationState = {
    activeTab: navigationList[0].name,
}

export const navigationSlice = createSlice({
    name: 'navigation',
    initialState,
    reducers: {
        changeActiveTab: (state, action: PayloadAction<string>) => {
            state.activeTab = action.payload
        },
    }
})

export const { changeActiveTab } = navigationSlice.actions

export default navigationSlice.reducer