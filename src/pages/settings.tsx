const Settings = () => {
    return (
        <div className="flex items-center justify-center h-full bg-gray-200 text-gray-800 p-8">
            <div className="grid w-full max-w-screen-md gap-8">
                <div>
                    <h2 className="text-medium font-medium mb-1 ml-1 border-b-2 border-b-gray-800">FTX</h2>
                    <p className="text-xs mb-2 ml-1 font-medium">Note: keys and secrets are stored in browser's local storage instead of a remote server for security.</p>
                    <div className="bg-gray-100 rounded shadow-lg">
                        <div className="grid gap-4 p-8">
                            <div className="">
                                <label className="text-xs font-semibold">API Key</label>
                                <input className="flex items-center h-10 border mt-1 rounded px-4 w-full text-sm" type="text" />
                            </div>
                            <div className="">
                                <label className="text-xs font-semibold">API Secret</label>
                                <input className="flex items-center h-10 border mt-1 rounded px-4 w-full text-sm" type="text" />
                            </div>
                            <button className="items-center justify-center bg-blue-600 text-sm font-medium w-full h-10 rounded text-blue-50 hover:bg-blue-700">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Settings