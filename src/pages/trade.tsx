import React from 'react';
import TickersList from '../features/tickerList/tickersList'
import TVChart from '../features/chart/chart';

const Main = () => {
    return (
        <body className="flex w-screen h-screen">
            <div className="flex flex-col w-64 border-r border-gray-300 py-3">
                <p>tickers list</p>   
                <TickersList />
            </div>
            <div className="flex-grow flex flex-col bg-indigo-200">
                <div className="flex flex-grow">
                    <TVChart />
                </div>
                <div className="flex flex-col h-64 pt-3 bg-gray-600">
                    <p className="self-center text-white">Positions</p>
                </div>
            </div>
            <div className="flex flex-col w-64 border-r border-gray-500 pl-3">
                Order Actions
            </div>

        </body>
    )
}

export default Main