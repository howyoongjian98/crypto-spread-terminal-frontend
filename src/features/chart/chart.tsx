import { AdvancedRealTimeChart } from "react-ts-tradingview-widgets";
import { useAppSelector } from '../../hooks';


const TVChart = () => {
    const {selectedTicker} = useAppSelector((state) => state.tickers)
    return (
        <div className="w-full">
            <AdvancedRealTimeChart 
                symbol={selectedTicker}
                interval="D" 
                theme="dark"
                save_image={false}
                allow_symbol_change={false}
                enable_publishing={false}
                style="9"
                hide_top_toolbar={true}
                autosize></AdvancedRealTimeChart>
        </div>
    )
}

export default TVChart