// import React from 'react';
import { useSelector, useDispatch } from 'react-redux'
import {RootState} from '../../store'
import {selectNewTicker} from './tickerSlice'

const TickersList = () => {
    const {tickersList} = useSelector((state: RootState) => state.tickers)
    const dispatch = useDispatch()

    return (
        <div className="flex flex-col items-start pl-3">
                {tickersList.map((ticker) => (
                    <div 
                        key={ticker}
                        onClick={()=> dispatch(selectNewTicker(ticker))} 
                        className="mb-1 border-b-2 border-b-indigo-500">
                            {ticker}
                    </div>
                ))}
        </div>
    )

}

export default TickersList