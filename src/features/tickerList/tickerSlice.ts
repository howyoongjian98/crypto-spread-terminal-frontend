import { createSlice, PayloadAction } from "@reduxjs/toolkit";

const defaultTickersList = [
    "FTX:CREAMPERP/FTX:AAVEPERP",
    "FTX:ARPERP/FTX:FILPERP",
    "FTX:ETHPERP/FTX:BTCPERP",
    "FTX:LUNAPERP/FTX:BTCPERP"
]

interface TickerState {
    selectedTicker: string,
    tickersList: string[]
}

const initialState: TickerState = {
    tickersList : defaultTickersList,
    selectedTicker: defaultTickersList[0]
}

export const tickerSlice = createSlice({
    name: 'ticker',
    initialState,
    reducers: {
        selectNewTicker: (state, action: PayloadAction<string>) => {
            state.selectedTicker = action.payload
        },
        addTickerToList: (state, action: PayloadAction<string>) => {
            state.tickersList.push(action.payload)
        },
    }
})

export const {selectNewTicker, addTickerToList} = tickerSlice.actions

export default tickerSlice.reducer